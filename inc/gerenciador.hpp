#ifndef GERENCIADORDEIMAGENS_HPP
#define GERENCIADORDEIMAGENS_HPP
#include <stdlib.h>		
#include <iostream>		
#include <fstream>		
#include <string>
#include <cstring>
#include "imagemCinza.hpp"
#include "imagemRGB.hpp"	
#include "decifraCinza.hpp"
#include "decifraRGB.hpp"
using namespace std;

class Gerenciador{
	public:

		void tratar_imagem_cinza();
		void tratar_imagem_colorida();
	private:
		//void verifica_formato(ofstream arquivo, string caminho_arquivo);
		void obter_segredo(imagemCinza * imagem);
		string obter_info_imagem(); 
		void imprime_oi();
		void verifica_formato(string caminho_arquivo, char numero_magico[3]);
};
#endif