#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <string>

using namespace std;
// Classe abstrata criada para conter Atributos das classes filhas e funções generiacas para pegar e retornar o valor dos atributos
class imagem
{
	// Atributos 
protected:
	char * numero_magico;	//Ponteiro para alocar Numero magico da imagem
	int * medidas;	//Ponteiro para alocar Dimensões da imagem
	int Profundidade;
	string parte_segredo;	
	char *** faixa;	
	// Metodos
public:
	char * get_numero_magico();	//Envia o numero magico
	void set_numero_magico(char * numero_magico);	//Seta o numero magico
	int * get_medidas();
	void setmedidas(int medidas[2]);	
	int getProfundidade();	
	void setProfundidade(int Profundidade);	
	string get_parte_segredo();	
	void setparte_segredo(string parte_segredo);	
	char *** get_faixa();	//Envia o ponteiro para o inicio do valor das cores
	imagem();	//Construtor virtual
	~imagem();	//Destrutor para liberar a memoria das variaveis alocadas dinamicamente
};

#endif